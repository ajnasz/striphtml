package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"

	"golang.org/x/net/html"

	"gitlab.com/ajnasz/striphtml/converthtml"
)

var sourceFile string
var configFile string

func init() {
	const (
		defaultSourceFile = "-"
		sourceFileUsage   = "Path to the HTML file, - for stdin"
		defaultConfigFile = ""
		configFileUsage   = "Path to the config.json file"
	)
	flag.StringVar(&sourceFile, "f", defaultSourceFile, sourceFileUsage)
	flag.StringVar(&configFile, "c", defaultConfigFile, configFileUsage)

	flag.Parse()
}

func xy(z *html.Tokenizer, out string) string {
	tagToken := z.Next()

	if tagToken == html.ErrorToken {
		err := z.Err()
		if err == io.EOF {
			return out
		}
		panic(err)
	}

	if tagToken == html.TextToken {
		return xy(z, out+string(z.Text()))
	}

	return xy(z, out)
}

func main() {
	jsonFile, err := os.Open(configFile)
	if err != nil {
		fmt.Fprint(os.Stderr, err.Error())
		os.Exit(1)
		return
	}
	defer jsonFile.Close()

	tagConfig, err := converthtml.GetConfig(jsonFile)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Can't open config: %v\n", err)
		os.Exit(1)
	}

	var reader io.Reader
	if sourceFile == "-" {
		reader = bufio.NewReader(os.Stdin)
	} else {
		fileReader, err := os.Open(sourceFile)

		if err != nil {
			fmt.Fprintf(os.Stderr, "Can't open HTML file: %v\n", err)
			os.Exit(1)
		}
		defer fileReader.Close()

		reader = fileReader
	}

	fmt.Print(converthtml.ConvertHTML(tagConfig, reader))
}
