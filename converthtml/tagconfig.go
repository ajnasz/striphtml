package converthtml

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// TagConfig data structuore to store configuration for a html tag
type TagConfig struct {
	TagName  string
	Template string
	Skip     bool
}

func readJSONFile(file io.Reader) ([]byte, error) {
	return ioutil.ReadAll(file)
}

func generateTagConfigs(jsonFile []byte) []TagConfig {
	var result []TagConfig

	json.Unmarshal(jsonFile, &result)

	return result
}

// GetConfig Reads config
func GetConfig(configFile io.Reader) ([]TagConfig, error) {
	jsonFile, err := readJSONFile(configFile)

	if err != nil {
		return nil, err
	}

	return generateTagConfigs(jsonFile), nil
}
