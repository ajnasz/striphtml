package converthtml

type stackItem struct {
	TagName string
	TagAttr map[string]string
	Text    string
	Depth   int
}
