package converthtml

import (
	"fmt"
	"golang.org/x/net/html"
	"io"
	"log"
	"os"
	"strings"
	"text/template"
)

func getTagConfigForTagName(tagConfigs []TagConfig, tagName string) *TagConfig {
	for _, config := range tagConfigs {
		if config.TagName == tagName {
			return &config
		}
	}

	return nil
}

func tplToText(tpl *template.Template, tag stackItem) string {
	var b strings.Builder
	tpl.Execute(&b, tag)
	out := b.String()
	return out
}

func tagToTelegramText(tagConfig []TagConfig, tag stackItem) string {
	skip := false
	tagName := tag.TagName
	var tpl *template.Template
	var err error
	config := getTagConfigForTagName(tagConfig, tag.TagName)

	if config == nil {
		return tag.Text
	}

	funcMap := template.FuncMap{
		"repeat": strings.Repeat,
		"trim":   strings.Trim,
	}

	tpl, err = template.New(tagName).Funcs(funcMap).Parse(config.Template)

	switch tagName {
	case "meta":
		skip = true
	}

	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}

	if skip {
		return ""
	}

	if tpl == nil {
		return tag.Text
	}

	return tplToText(tpl, tag)
}

func getDepth(stack []stackItem, tagName string) int {
	depth := 1
	for i := len(stack) - 1; i >= 0; i-- {
		if stack[i].TagName == tagName {
			depth++
		}
	}

	return depth
}

func getTagAttributes(token *html.Tokenizer) map[string]string {
	out := make(map[string]string)
	for {
		key, value, hasMore := token.TagAttr()
		out[string(key)] = string(value)

		if !hasMore {
			break
		}
	}

	return out
}

// ConvertHTML converts html input into output
func ConvertHTML(tagConfig []TagConfig, input io.Reader) string {
	stack := []stackItem{
		{TagName: "document"},
	}
	tagName := ""

	z := html.NewTokenizer(input)

	for {
		tagToken := z.Next()
		switch tagToken {
		case html.ErrorToken:
			err := z.Err()
			if err == io.EOF {
				return stack[len(stack)-1].Text
			}
			log.Fatal(err)
		case html.TextToken:
			text := string(z.Text())
			trimmed := strings.TrimSpace(text)
			if trimmed != "" {
				stack[len(stack)-1].Text += text
			}
		case html.StartTagToken, html.EndTagToken:
			tn, _ := z.TagName()
			tagName = string(tn)

			if tagToken == html.StartTagToken {
				stack = append(stack, stackItem{tagName, getTagAttributes(z), "", getDepth(stack, tagName)})
			} else if tagToken == html.EndTagToken {
				current := stack[len(stack)-1]
				stack = stack[:len(stack)-1]
				stack[len(stack)-1].Text += tagToTelegramText(tagConfig, current)
			}
		default:
			break
		}
	}
}
