package converthtml

import (
	"strings"
	"testing"
)

func TestConvertHTML(t *testing.T) {
	tagConfigs := []TagConfig{
		{
			TagName:  "h1",
			Template: "# {{.Text}}\n",
			Skip:     false,
		},
		{
			TagName:  "em",
			Template: "_{{.Text}}_",
		},
		{
			TagName:  "li",
			Template: "{{repeat \"  \" .Depth}}* {{.Text}}\n",
		},
		{
			TagName:  "ul",
			Template: "\n{{.Text}}",
		},
		{
			TagName: "head",
			Skip:    true,
		},
	}

	texts := []struct {
		Description string
		Text        string
		Expected    string
	}{
		{"h1 tag", "<h1>HEAD</h1>", "# HEAD\n"},
		{"h1 with inner tag", "<h1>Head <em>Embed</em></h1>", "# Head _Embed_\n"},
		{"Tag without template", "<strong>Tag without template</tag>", "Tag without template"},
		{"Skipped tag", "<head>It must be skipped</head>", ""},
		{"Skipped tag wit inner tag", "<head>It must be skipped <title>What about the title</title></head>", ""},
		{"Repeat funcs", "<ul><li>First level text<ul><li>Second level text</li></ul></li></ul>", "\n  * First level text\n    * Second level text\n\n"},
		{"Trim space", "<li>test</li>\n<li>test2</li>", "  * test\n  * test2\n"},
	}

	for _, i := range texts {
		reader := strings.NewReader(i.Text)
		actual := ConvertHTML(tagConfigs, reader)
		if actual != i.Expected {
			t.Errorf("%s: got •%s•, expected •%s•", i.Description, actual, i.Expected)
		}
	}

}
